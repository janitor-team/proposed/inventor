#!/usr/bin/make -f
# Sample debian/rules that uses debhelper.
# This file is public domain software, originally written by Joey Hess.
#
# This version is for a multibinary package. It also allows you to build any
# of the binary packages independantly, via binary-<package> targets.

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

export DEB_BUILD_MAINT_OPTIONS=hardening=+all

# This has to be exported to make some magic below work.
export DH_OPTIONS

export IVROOT=$(shell pwd)/debian/tmp
export FREETYPE=1

# Set optimization and debugging flags [Policy 10.1].
# These options are controlled by variables VCOPTS and VCXXOPTS;
# see setting of "CFLAGS" in make/commondefs for details.
#
# Since we're building libraries, set _REENTRANT [Policy 10.2].
#
export VCOPTS=-g -D_REENTRANT

ifneq (,$(findstring noopt,$(DEB_BUILD_OPTIONS)))
VCOPTS += -O0
else
VCOPTS += -O3
endif

export VCXXOPTS=$(VCOPTS)

# Set linking flags.
# These options are controlled by the variable VLDOPTS;
# see setting of "LDFLAGS" in make/commondefs for details.
# export VLDOPTS=-Wl,--as-needed

# Sadly, the program "ivman" used to generate manual pages
# is broken on several architectures.  It looks to me like
# there are assumptions being made about the form of mangled
# identifiers, or maybe some bad assumptions being broken on 64-bit
# architectures.  Whatever.
#
# The man pages are therefore only generated on certain architectures,
# but since it is an architecture "all" package, we only have to
# ensure that the uploader builds on a favoured architecture, e.g.
# i386.
#
# NMU note: "all" is now built on an amd64 buildd. Lets just hope
# ivman works on amd64; hardcoding a list of archs that are not used
# to build "all" certainly does not help.
ifneq (,$(findstring $(DEB_BUILD_ARCH),amd64 i386 sparc powerpc mips mipsel))
export BUILDMAN=true
endif


build: build-stamp
build-stamp:
	dh_testdir

	# libFL binary archive changed name in CVS version
	rm -f libFL/src/libFL*.a

	install -d $(IVROOT)
	LD_LIBRARY_PATH=$(IVROOT)/usr/lib dh_auto_build -- install
	# Fix weird permissions in upstream source.
	chmod -x apps/examples/Toolmaker/08.Manips/README

	touch build-stamp

clean:
	dh_testdir
	dh_testroot
	rm -f build-stamp
	-$(MAKE) -k clobber
	# Clear out symlinks created during build.
	rm -f libFL/ang/flclient.h libFL/freetype lib/libInventor.so.1 libSoXt/libInventorXt.so.1
	dh_clean

install: DH_OPTIONS=
install: build
	dh_testdir
	dh_testroot
	#dh_clean -k
	dh_installdirs

	# Add here commands to install the package into debian/tmp.
	# should we re-do make install here?
	$(MAKE) install
	cp -a data/textures debian/tmp/usr/share/inventor/data/textures
	rm -rf debian/tmp/usr/share/inventor/data/textures/CVS
	install -d debian/tmp/usr/share/inventor/fonts
	install debian/link-fonts.sh debian/tmp/usr/share/inventor/fonts

# Ensure the man directories are created
	mkdir -p debian/tmp/usr/share/man/man1
	mkdir -p debian/tmp/usr/share/man/man3

# Remove SceneViewer from the demo install
	rm debian/tmp/usr/lib/inventor/SceneViewer*

	dh_movefiles
# Clean up the directories used as examples.
	-$(MAKE) -C apps/examples clobber
	-$(MAKE) -C apps/nodes clobber
	-$(MAKE) -C apps/samples clobber
	-$(MAKE) -C apps/tools/ivAddVP clobber
	-$(MAKE) -C apps/tools/ivnorm clobber
	-$(MAKE) -C apps/tools/ivperf clobber
	dh_install

# Fix up path names so demo apps can find their data files.
	cd debian/inventor-demo/usr/share/doc/inventor-demo/src/apps/examples/Mentor/CXX \
	  && perl -i -ple 's!/usr/share/src/Inventor/examples/!../../!' *.c++


# This single target is used to build all the packages, all at once, or
# one at a time. So keep in mind: any options passed to commands here will
# affect _all_ packages. Anything you want to only affect one package
# should be put in another target, such as the install target.
binary-common:
	dh_testdir
	dh_testroot
#	dh_installdebconf
	dh_installdocs
	dh_installexamples
	dh_installmenu
	dh_installchangelogs
	dh_lintian
	dh_link
	dh_compress
	dh_fixperms
	dh_strip -XGNUmakefile.libInventorWidget.a
	dh_makeshlibs
	dh_installdeb
	dh_shlibdeps -L libinventor1 -l debian/tmp/usr/lib
	dh_gencontrol
	dh_md5sums
	dh_builddeb

# Build architecture independant packages using the common target.
binary-indep: build install
	 $(MAKE) -f debian/rules DH_OPTIONS=-i binary-common

build-indep: build

# Build architecture dependant packages using the common target.
binary-arch: build install
	$(MAKE) -f debian/rules DH_OPTIONS=-a binary-common

build-arch: build

# Any other binary targets build just one binary package at a time.
binary-%: build install
	make -f debian/rules binary-common DH_OPTIONS=-p$*

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install
