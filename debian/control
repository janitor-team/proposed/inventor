Source: inventor
Maintainer: Debian Science Team <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Steve M. Robbins <smr@debian.org>
Section: devel
Priority: optional
Build-Depends: debhelper-compat (= 13),
               csh,
               libx11-dev,
               libxi-dev,
               libxt-dev,
               x11proto-core-dev,
               libmotif-dev,
               libglw1-mesa-dev,
               libglu1-mesa-dev,
               libfreetype6-dev,
               libjpeg-dev,
               bison
Standards-Version: 4.6.0
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/science-team/inventor
Vcs-Git: https://salsa.debian.org/science-team/inventor.git
Homepage: http://oss.sgi.com/projects/inventor/

Package: libinventor1
Architecture: any
Section: libs
Priority: optional
Depends: ${misc:Depends},
         ${shlibs:Depends},
         xfonts-scalable,
         gsfonts-x11
Recommends: xdg-utils,
            xpdf | pdf-viewer
Conflicts: libinventor0
Replaces: libinventor0
Description: Open Inventor runtime environment
 This package contains the required files to run Open Inventor applications.
 .
 Open Inventor is an object-oriented 3D toolkit offering a comprehensive
 solution to interactive graphics programming problems.  It presents a
 programming model based on a 3D scene database that simplifies graphics
 programming.  It includes a large set of objects such as cubes, polygons,
 text, materials, cameras, lights, trackballs, handle boxes, 3D viewers, and
 editors can speed up your programming and extend your 3D program's
 capabilities.
 .
 Recommended package xpdf is needed only to view help files.

Package: inventor-dev
Architecture: any
Section: devel
Priority: optional
Depends: ${misc:Depends},
         libinventor1 (= ${binary:Version}),
         libc6-dev,
         xlibmesa-dev | libgl-dev,
         libmotif-dev
Conflicts: libcoin20-dev,
           libcoin20-doc
Description: Open Inventor development files
 This package contains the files required to develop Open Inventor
 applications.
 .
 Open Inventor is an object-oriented 3D toolkit offering a comprehensive
 solution to interactive graphics programming problems.  It presents a
 programming model based on a 3D scene database that simplifies graphics
 programming.  It includes a large set of objects such as cubes, polygons,
 text, materials, cameras, lights, trackballs, handle boxes, 3D viewers, and
 editors can speed up your programming and extend your 3D program's
 capabilities.

Package: inventor-doc
Architecture: all
Section: doc
Priority: optional
Depends: ${misc:Depends}
Conflicts: libcoin20-doc,
           libcoin40-doc
Description: Open Inventor documentation
 This package contains the Open Inventor documentation files.
 .
 Open Inventor is an object-oriented 3D toolkit offering a comprehensive
 solution to interactive graphics programming problems.  It presents a
 programming model based on a 3D scene database that simplifies graphics
 programming.  It includes a large set of objects such as cubes, polygons,
 text, materials, cameras, lights, trackballs, handle boxes, 3D viewers, and
 editors can speed up your programming and extend your 3D program's
 capabilities.

Package: inventor-clients
Architecture: any
Section: graphics
Priority: optional
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: Open Inventor client programs
 This package contains Open Inventor file viewers and converters.
 .
 Open Inventor is an object-oriented 3D toolkit offering a comprehensive
 solution to interactive graphics programming problems.  It presents a
 programming model based on a 3D scene database that simplifies graphics
 programming.  It includes a large set of objects such as cubes, polygons,
 text, materials, cameras, lights, trackballs, handle boxes, 3D viewers, and
 editors can speed up your programming and extend your 3D program's
 capabilities.

Package: inventor-data
Architecture: all
Section: graphics
Priority: optional
Depends: ${misc:Depends}
Description: Open Inventor sample data files
 This package contains sample 3D model and scene files in inventor format.
 .
 Open Inventor is an object-oriented 3D toolkit offering a comprehensive
 solution to interactive graphics programming problems.  It presents a
 programming model based on a 3D scene database that simplifies graphics
 programming.  It includes a large set of objects such as cubes, polygons,
 text, materials, cameras, lights, trackballs, handle boxes, 3D viewers, and
 editors can speed up your programming and extend your 3D program's
 capabilities.

Package: inventor-demo
Architecture: any
Section: graphics
Priority: optional
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: Open Inventor demonstration programs and example code
 This package includes several Open Inventor demos: a scene viewer
 (SceneViewer), a scene graph viewer and editor (gview), a maze game (maze),
 an extrusion editor (noodle), a 3D morphing tool (qmorf), a surface of
 revolution editor (revo), among others.
 .
 This package also contains example code, including the examples
 from the books "The Inventor Mentor", and "The Inventor Toolmaker".
 .
 Open Inventor is an object-oriented 3D toolkit offering a comprehensive
 solution to interactive graphics programming problems.  It presents a
 programming model based on a 3D scene database that simplifies graphics
 programming.  It includes a large set of objects such as cubes, polygons,
 text, materials, cameras, lights, trackballs, handle boxes, 3D viewers, and
 editors can speed up your programming and extend your 3D program's
 capabilities.
