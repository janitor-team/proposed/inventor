		DIRECTORY LAYOUT
		----------------

The layout of demos, fonts, and data has been changed to reflect
Debian practice.

The following subdirectories of /usr/share/inventor are used for
Inventor:

	/fonts - font files are searched for here
	/help - on-line help files for demos & applications
	/data - subdirectories for models, materials, textures

The inventor-demos package installs its demos in /usr/lib/inventor,
and looks for models and other data in /usr/share/inventor/data/demos.



		FONTS
		-----

When a font is requested, Inventor will check in the directory
/usr/share/inventor/fonts for a file with the name of the font.  

No fonts are shipped with the code, however.  Rather, the scalable
fonts from XFree86 (xfonts-scalable) and ghostscript (gsfonts-x11) are
used.  The files in /usr/share/inventor/fonts are all symbolic links
to the font file in /usr/lib/X11/fonts/Type1.  The links are made in
using the script /usr/share/inventor/fonts/link-fonts.sh in the post
install phase.  Should these links become broken (e.g. if the target
of the link is removed), please file a bug, and re-run the script by
hand.

Inventor will fall back to Utopia-Regular, if the requested font is
not available.  Odd things may happen if Utopia-Regular is not found.
The link-fonts.sh script endeavours to ensure that a link of this name
is always available.  Please do the same if you modify the links.



		KNOWN BUGS
		----------

There is a bug in the font handling code that causes inventor
to misbehave when displaying a string containing an embedded space.
I have added a kludge to the code that simply sets the width of a
space to be 10.  If the spacing looks odd, this may be the reason
why.  A fix would be welcome.



		THANKS
		------

Many thanks to Marcelo E. Magallon for providing me with his
unreleased packaging of this code.  He showed me how to properly build
the shared objects.  Many thanks also to Eric Sharkey and Steve
Langasek for setting me straight on the Debian policy for SONAMES.

